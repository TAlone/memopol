# coding: utf-8

from django.contrib import admin

import nested_admin

from representatives_recommendations.models import Recommendation
from representatives_recommendations.forms import RecommendationForm

from .models import Dossier, Document, Proposal, Vote


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('dossier_reference', 'kind', 'title', 'link')
    search_fields = ('reference', 'dossier__reference', 'title')

    def dossier_reference(self, obj):
        return obj.dossier.reference


class RecommendationStackedInline(nested_admin.NestedStackedInline):
    model = Recommendation
    min_num = 1
    max_num = 1
    fields = ('title', 'description',
            'recommendation', 'weight',
            'id', 'proposal',)
    form = RecommendationForm


class ProposalAdmin(nested_admin.NestedModelAdmin):
    list_display = (
        'reference',
        'dossier_reference',
        'title',
        'kind')
    search_fields = ('reference', 'dossier__reference', 'title')
    inlines = [
        RecommendationStackedInline,
    ]

    def dossier_reference(self, obj):
        return obj.dossier.reference


class ProposalStackedInline(nested_admin.NestedStackedInline):
    model = Proposal
    extra = 0
    can_delete = False
    min_num = 0
    readonly_fields = ('title', 'description',)
    fields = ('title', 'description',)
    show_change_link = True
    inlines = [
        RecommendationStackedInline,
    ]


class DossierAdmin(nested_admin.NestedModelAdmin):
    list_display = ('id', 'reference', 'title')
    search_fields = ('reference', 'title')
    inlines = [
        ProposalStackedInline,
    ]


class NoneMatchingFilter(admin.SimpleListFilter):
    title = 'Representative'
    parameter_name = 'representative'

    def lookups(self, request, model_admin):
        return [('None', 'Unknown')]

    def queryset(self, request, queryset):
        if self.value() == 'None':
            return queryset.filter(representative=None)
        else:
            return queryset


class VoteAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'proposal_reference',
        'position',
        'representative',
        'representative_name')
    list_filter = (NoneMatchingFilter,)

    def proposal_reference(self, obj):
        return obj.proposal.reference


admin.site.register(Dossier, DossierAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Proposal, ProposalAdmin)
admin.site.register(Vote, VoteAdmin)
